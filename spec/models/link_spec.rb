require 'rails_helper'

RSpec.describe Link, type: :model do

  before do
    stub_request(:any, 'http://example.org')
    stub_request(:any, 'http://idontexist.org')
      .to_return(status: 404)
  end

  context 'validation' do
    let(:link) { build(:link) }

    it 'requires a url' do
      expect(link).to validate_presence_of(:url)
    end

    it 'slug must be unique' do
      expect(link).to validate_uniqueness_of(:slug)
    end

  end

  describe 'created with valid url' do
    let!(:valid_link) { build(:link)}

    it 'should be valid' do
      expect(valid_link).to be_valid
    end

  end

  describe 'created with invalid url' do
    let(:invalid_link) { build(:link, url: 'idontexist.org') }

    it 'should not be valid' do
      expect(invalid_link).not_to be_valid
    end

  end

  describe '.slug_available?' do
    let!(:link) do
      create(:link, slug: 'fish')
    end

    it 'returns true for available slugs' do
      expect(Link.slug_available?('tuna')).to be_truthy
    end

    it 'returns false for existing slugs' do
      expect(Link.slug_available?('fish')).to be_falsey
    end

  end
end
