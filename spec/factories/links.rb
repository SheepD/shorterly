FactoryGirl.define do
  factory :link do
    url 'http://example.org'
    slug Faker::Internet.slug
  end
end
