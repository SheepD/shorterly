Rails.application.routes.draw do
  root to: 'app#index'
  get 'app' => 'app#index'
  get 'app/*' => 'app#index'
  get 'l/:slug' => 'links#redirect'

  namespace :api do
    resources :links, except: [:edit, :new] do
      collection do
        get 'available' => 'links#available'
      end
    end
  end

  get '*unmatched_route', to: 'application#not_found'
end
