const { environment } = require('@rails/webpacker')
const webpack = require('webpack')

environment.plugins.set(
  'Provide',
  new webpack.ProvidePlugin({
    axios: 'axios',
    _: 'lodash',
  })
)

module.exports = environment
