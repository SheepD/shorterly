import '../globals/style'
import '../globals/axios'

import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import AppLayout from '../components/AppLayout'

import PropTypes from 'prop-types'

const Hello = props => (
  <div>Hello {props.name}!</div>
)

Hello.defaultProps = {
  name: 'David'
}

Hello.propTypes = {
  name: PropTypes.string
}

document.addEventListener('DOMContentLoaded', () => {
  render(
    <Router>
      <AppLayout />
    </Router>,
    document.body.appendChild(document.createElement('div')),
  )
})
