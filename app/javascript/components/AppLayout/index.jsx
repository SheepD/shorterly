import './style'
import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import { blue700, blue900 } from 'material-ui/styles/colors'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import AppBar from 'material-ui/AppBar'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import LinkIndex from '../LinkIndex'

function AppLayout () {
  const muiTheme = getMuiTheme({
      palette: {
        primary1Color: blue700,
        primary2Color: blue900,
      }
    })

  return (
    <MuiThemeProvider muiTheme={muiTheme}>
      <div className="main-body">
        {/* header */}
        <AppBar title="Shorterly" />

        {/* body */}
        <div className="main-content">
          <Route
            exact
            path="/"
            component={LinkIndex}
          />
        </div>

        {/* footer */}
        <footer className="footer">
          &copy; Maru de Vera. 2017
        </footer>
      </div>
    </MuiThemeProvider>
  )
}

export default AppLayout
