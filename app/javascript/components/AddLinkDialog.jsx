import React, { Component } from 'react'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import FontIcon from 'material-ui/FontIcon'
import IconButton from 'material-ui/IconButton'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'

class AddLinkDialog extends Component {
  constructor (props) {
    super(props)
    this.state = {
      open: false,
      url: '',
      slug: '',
      submitted: false,
      errorMessage: '',
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.valid = this.valid.bind(this)
  }

  handleChange (e) {
    this.setState({url: e.target.value})
  }

  handleClose () {
    this.setState({
      open: false,
      url: '',
      slug: '',
      submitted: false,
      errorMessage: '',
    })
  }

  handleOpen () {
    this.setState({open: true})
  }

  handleSubmit () {
    if (!this.valid()) return

    const apiURL = '/api/links'
    const { url } = this.state
    const data = {
      url: url
    }

    axios
    .post(apiURL, data)
    .then(response => {
      const { slug } = response.data
      this.setState({
        slug: slug,
        submitted: true,
      })
      this.props.onNewLink({url: url, slug: slug})
    })
  }

  valid () {
    const { url } = this.state
    if (url && url.length <= 0) {
      this.setState({errorMessage: 'URL can\t be blank'})
      return false
    }

    const regex = /^https?:\/\//
    if (!regex.test(url)) {
      this.setState({errorMessage: 'Invalid URL'})
      return false
    }

    this.setState({errorMessage: ''})
    return true
  }

  render () {
    const {
      errorMessage,
      open,
      slug,
      submitted,
      url,
    } = this.state
    const actions = [
      <FlatButton
        label="Cancel"
        onClick={this.handleClose}
      />
    ]
    if (!submitted)
      actions.push(
        <RaisedButton
          label="Create"
          primary={true}
          onClick={this.handleSubmit}
        />
      )

    return (
      <div>
        <IconButton
          onClick={this.handleOpen}
          tooltip="Add"
          tooltipPosition="top-center"
          touch={true}
        >
          <FontIcon className="material-icons">
            add
          </FontIcon>
        </IconButton>
        <Dialog
          actions={actions}
          modal={false}
          onRequestClose={this.handleClose}
          open={open}
          title="Shorten New URL"
        >
          {submitted ? (
            <div>
              <p>Shortened URL has been created!</p>
              <p>
                <a href={`${window.location.host}/l/${slug}`}>
                  {`${window.location.host}/l/${slug}`}
                </a>
              </p>
            </div>
          ) : (
            <TextField
              floatingLabelText="URL"
              errorText={errorMessage}
              onChange={this.handleChange}
              value={url}
            />
          )}
        </Dialog>
      </div>
    )
  }
}

export default AddLinkDialog
