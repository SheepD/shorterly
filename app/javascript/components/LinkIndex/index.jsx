import './style'
import AddLinkDialog from '../AddLinkDialog'
import React, { Component } from 'react'

class LinkIndex extends Component {
  constructor (props) {
    super(props)
    this.state = {
      query: '',
      page: 1,
      links: []
    }

    this.fetchLinks = this.fetchLinks.bind(this)
    this.handleRowClick = this.handleRowClick.bind(this)
    this.handleNewLink = this.handleNewLink.bind(this)
  }

  componentDidMount () {
    this.fetchLinks()
  }

  fetchLinks () {
    const url = '/api/links'

    axios
    .get(url)
    .then(response => {
      this.setState({links: response.data})
    })
  }

  handleRowClick (link) {
    this.props.history.push(`/link/${link.slug}`)
  }

  handleNewLink (link) {
    const { links } = this.state
    links.push(link)
    this.setState({links: links})
  }

  render () {
    const { links } = this.state
    const tableBody =
      links.map(link => {
        return (
          <div
            className="table-row"
            key={`link-${link.id}`}
            onClick={() => { this.handleRowClick(link) }}
          >
            <div className="table-column">
              {_.truncate(link.url, {lenght: 30})}
            </div>
            <div className="table-column">
              {`${window.location.host}/l/${link.slug}`}
            </div>
          </div>
        )
      })
    return (
      <div>
        <div className="page-header">
          <div className="title">Links</div>
          <div className="option">
            <AddLinkDialog onNewLink={this.handleNewLink} />
          </div>
        </div>

        <div className="table link-table">
          <div className="table-header">
            <div className="table-row">
              <div className="table-column">
                URL
              </div>
              <div className="table-column">
                Shortened URL
              </div>
            </div>
          </div>
          <div className="table-body">
            {tableBody}
          </div>
        </div>
      </div>
    )
  }
}

export default LinkIndex
