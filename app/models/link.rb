class Link < ApplicationRecord
  validates :url, presence: true
  validates :slug, uniqueness: true, presence: true
  validate :url_exists
  before_validation :set_slug

  def set_slug
    self.slug = Link.generate_slug if self.slug.blank? && new_record?
  end

  def url_exists
    self.url = 'http://' + url unless url  =~ /^http:\/\//
    if Faraday.get(self.url).status >= 400
      errors.add(:url, 'must exist')
    end
  rescue => e
    errors.add(:url, 'must be valid')
  end


  def self.generate_slug
    slug = SecureRandom.hex(6)
    while !self.slug_available? slug do
      slug = SecureRandom.hex(6)
    end

    return slug
  end

  def self.slug_available? string
    Link.where(slug: string).empty?
  end
end
