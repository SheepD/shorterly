class API::LinksController < ApplicationController
  before_action :set_params, only: [:create, :update, :available]
  before_action :set_link, only: [:showm, :update, :destroy]

  def index
    links = Link.all
    render json: links
  end

  def show
    render json: @link
  end

  def create
    link = Link.create @params
    if link
      render json: link
    else
      render json: link.errors, status: 422
    end
  end

  def update
    if @link.update @params
      render json: @link
    else
      render json: @link.errors, status: 422
    end
  end

  def destroy
    if @link.destroy
      render json: @link
    else
      render json: @link.errors, status: 422
    end
  end

  def available
    slug = @params[:slug]
    available = Link.slug_available? slug
    render json: { available: available }
  end

  private

  def set_params
    @params = params.permit(:url, :slug)
  end

  def set_link
    @link = Link.find params[:id]
  end
end
