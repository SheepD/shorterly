class LinksController < ApplicationController
  before_action :set_link

  def redirect
    redirect_to @link.url
  end

  private

  def set_link
    @link = Link.find_by slug: params[:slug]
  end
end
