class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def not_found
    redirect_to root_url
  end
end
